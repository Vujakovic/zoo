import React, {useState} from 'react';


function AnimalList() {

    const [animals, setAnimals] = useState([

        {   id:1,
            vrsta: 'Mačka - Lav',
            ime: 'Simba',
            datumRodjenja: new Date().toLocaleDateString()
        },
        {   id:2,
            vrsta: 'Pas - Hijena',
            ime: 'Roki',
            
        },
        {   id:3,
            vrsta: 'Slon',
            ime: 'Dambo',
            datumRodjenja: new Date().toLocaleDateString(),
            sektor:'slonovi'
        },
    ]);

    const sektori = ['ptice', 'zmije', 'mačke', 'slonovi', 'konji']

    const [vrsta, setVrsta] = useState("")
    const [ime, setIme] = useState("")
    const [datumRodjenja, setDatumRodjenja] = useState("")
    const [sektor, setSektor] = useState("ptice")



    const removeAnimal = (id) => {
       setAnimals(animals.filter(animal=>animal.id !==id))
    }


    const moveToTop = (animal,index)=>{
       
        const animalsCopy = [...animals]

       animalsCopy.splice(index,1)
       const newArr = [animal, ...animalsCopy]

       setAnimals(newArr)
    }


   

    const addAnimal = (event) => {
        event.preventDefault()

        const ids = animals.map(animal => animal.id);

        const max = Math.max(...ids);

        const newAnimal = {
            id: max+1,
            vrsta: vrsta,
            ime: ime,
            datumRodjenja: new Date(datumRodjenja).toLocaleDateString(),
            sektor: sektor
        }

        setAnimals([...animals, newAnimal])
        
    }

    const showSectorAnimals = (sektor)=>{
        
        alert(animals.filter(animal=>animal.sektor===sektor).map(animal=>animal.ime))
       
    }
    

    const getDatumRodjenja = datum => datum ? datum : 'Nepoznat'
    const getSektor = sektor => sektor ? sektor : 'Nepoznat'

    
  return (
    <div className="AnimalList">
        <div className="formAddAnimal container mb-5">
            <h1>Dodaj novu životinju</h1>
        <form onSubmit={addAnimal}>
            <div className="form-group">
                <label htmlFor="vrsta">Vrsta</label>
                <input value={vrsta} 
                        onChange={e => setVrsta(e.target.value)} 
                        type="text" className="form-control" id="vrsta"  
                        placeholder="Vrsta"/>
            </div>
            <div className="form-group">
                <label htmlFor="ime">Ime</label>
                <input value={ime} 
                        onChange={e => setIme(e.target.value)} 
                        type="text" className="form-control" 
                        id="ime"  placeholder="Ime"/>
            </div>
            <div className="form-group">
                <label htmlFor="datumRodjenja">Datum Rođenja</label>
                <input required value={datumRodjenja} 
                        onChange={e => setDatumRodjenja(e.target.value)} 
                        type="date" className="form-control" id="datumRodjenja"  
                        placeholder="Datum rođenja"/>
            </div>
            <div className="form-group">
                <label htmlFor="sektor"></label>
                <select name="sektori" id="" onChange={e=>setSektor(e.target.value)}>
                    {sektori.map((sektor,index)=>
                        <option key={index} value={sektor}>{sektor}</option>
                        )}
                </select>
            </div>
        
            <button type="submit" className="btn btn-primary">Dodaj životinju</button>
        </form>
        </div>

        <table className='table table-dark'>
            <thead>
                <tr>
                    <th>Sektor</th>
                    <th>Vrsta</th>
                    <th>Ime</th>
                    <th>Datum rođenja</th>
                </tr>
            </thead>

            <tbody>
                {animals.map((animal,index)=> (
                    <tr key={animal.id} > 
                        <td>{getSektor(animal.sektor)}</td>
                        <td>{animal.vrsta}</td>
                        <td>{animal.ime}</td>
                        <td>{getDatumRodjenja(animal.datumRodjenja)}</td>
                        <td><button className='btn btn-danger' onClick={()=>removeAnimal(animal.id)}>Remove</button></td>
                        <td>
                            {index!==0 && <button className="btn btn-success" onClick={()=>moveToTop(animal,index)}>Move to top</button>}
                            
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>

       
       <table className="tabela-sektora table table-striped">
           <thead>
               <tr>
                   <th>Sektor</th>
                   <th>Spisak životinja</th>
               </tr>
           </thead>

           <tbody>
               {sektori.map((sektor,index)=>
                <tr key={index}>
                    <td>{sektor}</td>
                    <td><button onClick={()=>showSectorAnimals(sektor)} className="btn btn-info">Spisak</button></td>
                </tr>
                )}
           </tbody>
       </table>
    </div>
  );
}

export default AnimalList;